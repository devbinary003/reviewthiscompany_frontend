import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import axios from 'axios';

import * as serviceWorker from './serviceWorker';

import App from './components/App';
import configureStore from './store/configureStore';
import { initUser } from './actions/userActions';

import './css/bootstrap.min.css';
import './css/style.css';

export const scAxios = axios.create({
    baseURL: 'https://reviewthiscompany.com/reviewgrowthapi/api/',
});

export const thirdparty = axios.create({
    baseURL: 'http://',
});


const store = configureStore();
store.dispatch(initUser());

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
