export const REG_PAGE_PATH = '/home';
export const TWENTY_MINUTES_TIME_IN_MILLISECONDS = '1200000';
export const API_TOKEN_NAME = 'api_token';
export const API_TOKEN_EXPIRY_NAME = 'api_token_expiry';
export const USER_ROLE = 'USER_ROLE';
export const USER_ID = 'USER_ID';
export const USER_EMAIL = 'USER_EMAIL';
export const IS_ACTIVE = 'IS_ACTIVE';

export const IMAGE_URL = 'https://reviewthiscompany.com/reviewgrowthapi/public/';
export const BASE_URL = 'https://reviewthiscompany.com/reviewgrowthapi/api/'; 
export const PROFILE_URL = 'https://reviewthiscompany.com';


