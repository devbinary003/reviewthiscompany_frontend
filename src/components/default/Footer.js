import React, { Component } from 'react';
import logoWhite from '../../images/logo-white.png';

class Footer extends React.Component {

    render() {

        return (  
        <>  
        <nav className="navbar navbar-expand-sm bg-dark navbar-dark fixed-bottom">
        <div className="container-fluid">
        <a className="navbar-brand" href="javascript:void(0)"><img src={logoWhite} /></a>
        <div className="collapse navbar-collapse" id="navb">
          <ul className="navbar-nav mr-auto">
          </ul>
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link" href="javascript:void(0)">Copyright © 2019 - ReviewThisCompany.com</a>
            </li>
          </ul>
        </div>
      </div>
      </nav>
        </>
        );
    }
}

export default Footer;
