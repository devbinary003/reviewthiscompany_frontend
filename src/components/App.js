import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import * as userActions from '../actions/userActions';
import PrivateRoute from './PrivateRoute';
import home from './default/home'; 
import postreview from './default/postreview'; 

import './App.css'; 

class App extends Component {
  render() { 
    return (
     <Router> 
        <Switch>
          <Route exact path="/" component={home} />
          <Route path='/:businessname' component={postreview} />
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = (state /*, ownProps*/) => {
  return {
    user: state.user,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(userActions, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);